package com.aceinteract.blipsit.entity

import org.hibernate.annotations.LazyCollection
import org.hibernate.annotations.LazyCollectionOption
import java.time.LocalTime
import javax.persistence.*

@Entity
data class Merchant (

        var name: String,

        @Column(unique = true)
        var slug: String,

        var description: String,
        var address: String,
        var latitude: Double,
        var longitude: Double,
        var openingTime: LocalTime,
        var closingTime: LocalTime,

        @ManyToOne
        var category: MerchantCategory,

        @ManyToMany
        @LazyCollection(LazyCollectionOption.FALSE)
        var itemCategories: MutableList<ItemCategory> = ArrayList(),

        @OneToMany(mappedBy = "merchant")
        @LazyCollection(LazyCollectionOption.FALSE)
        var items: MutableList<Item> = ArrayList()

) : BaseEntity() {

    val minTimeInMinutes: Int
        get() = items.sortedBy { it.durationInMinutes }[0].durationInMinutes + 30

    val maxTimeInMinutes: Int
        get() = items.sortedBy { it.durationInMinutes }.last().durationInMinutes + 40

}

@Entity
data class MerchantCategory (

        var name: String,

        @Column(unique = true)
        var slug: String,

        var description: String = ""

) : BaseEntity()