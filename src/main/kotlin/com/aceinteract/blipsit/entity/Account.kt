package com.aceinteract.blipsit.entity

import com.aceinteract.blipsit.BlipsitApplication
import com.aceinteract.blipsit.repository.OrderItemRepository
import com.aceinteract.blipsit.util.getHashValue
import org.hibernate.annotations.LazyCollection
import org.hibernate.annotations.LazyCollectionOption
import javax.persistence.*

@Entity
data class Account (

        @Column(unique = true)
        var email: String,

        @Transient
        private val rawPassword: String,

        var fullName: String,

        var bio: String = "",
        var gcmId: String? = null,
        var twitterId: String? = null,
        var facebookId: String? = null,
        var isActive: Boolean = true,
        var isLocked: Boolean = false,

        @OneToMany(fetch = FetchType.EAGER, mappedBy = "account")
        var addresses: MutableList<Address> = ArrayList(),

        @OneToMany(mappedBy = "account")
        @LazyCollection(LazyCollectionOption.FALSE)
        var cards: MutableList<Card> = ArrayList(),

        @OneToMany(mappedBy = "consumer")
        var consumerOrders: MutableList<ConsumerOrder> = ArrayList()

) : BaseEntity() {

    @OneToOne
    val basket: Basket = Basket(this)

    var password: String = ""

    init {
        if (rawPassword != "") {
            password = getHashValue("SHA-256", rawPassword)
        }
    }

}

@Entity
data class Card (

        @ManyToOne
        var account: Account,

        var cardName: String,

        var cardNumber: String,

        var cvv: String,

        var expiryMonth: String,

        var expiryYear: String

) : BaseEntity() {

//    @Transient
//    val maskedCardNumber
//        get() = "• • • " + cardNumber.substring(cardNumber.length - 4)

}

@Entity
data class Address (

        @ManyToOne
        var account: Account,

        var latitude: Double,
        var longitude: Double,

        var address: String,
        var extraInfo: String,
        var phoneNumber: String

) : BaseEntity()

@Entity
data class Basket (

        @OneToOne
        var consumer: Account,

        @OneToMany(mappedBy = "basket")
        @LazyCollection(LazyCollectionOption.FALSE)
        var items: MutableList<OrderItem> = ArrayList()

) : BaseEntity() {

    val merchant: Merchant?
        get() {
            return if (items.isNotEmpty()) items[0].item.merchant
            else null
        }

    fun getSubTotal(orderItemRepository: OrderItemRepository): Double {

        val basketItems = orderItemRepository.findAllByBasket(this)

        var basketSubTotal = 0.0
        basketItems.forEach {
            basketSubTotal += it.totalPrice
        }

        return basketSubTotal

    }

    fun getTotal(orderItemRepository: OrderItemRepository): Double {
        return getSubTotal(orderItemRepository) +
                (BlipsitApplication.STANDARD_DELIVERY_FEE * BlipsitApplication.DELIVERY_MULTIPLIER)
    }

}