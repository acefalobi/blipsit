package com.aceinteract.blipsit.entity

import org.hibernate.annotations.LazyCollection
import org.hibernate.annotations.LazyCollectionOption
import javax.persistence.*

@Entity
data class ConsumerOrder (

        @ManyToOne
        var consumer: Account,

        var paymentMethod: String,

        @ManyToOne
        var address: Address,

        @OneToMany(mappedBy = "order")
        @LazyCollection(LazyCollectionOption.FALSE)
        var items: MutableList<OrderItem> = ArrayList(),

        var deliveryStatus: String = DELIVERY_REQUESTING_ORDER

) : BaseEntity() {

    companion object {

        const val PAYMENT_CARD = "PAYMENT_CARD"
        const val PAYMENT_ON_DELIVERY = "PAYMENT_ON_DELIVERY"

        const val DELIVERY_REQUESTING_ORDER = "DELIVERY_REQUESTING_ORDER"
        const val DELIVERY_PREPARED_DELIVERY = "DELIVERY_PREPARED_DELIVERY"
        const val DELIVERY_DELIVERING_ORDER = "DELIVERY_DELIVERING_ORDER"

    }

}

@Entity
data class OrderItem (

        @ManyToOne
        var item: Item,

        @OneToMany(cascade = [CascadeType.REMOVE])
        var options: MutableList<OrderItemOption>,

        var quantity: Int,

        @ManyToOne
        var order: ConsumerOrder? = null,

        @ManyToOne
        var basket: Basket? = null

) : BaseEntity() {

    val totalPrice: Double
        get() {
            var total = item.price

            options.forEach {
                it.selections?.let { it1 ->
                    it1.forEach { it2 -> total += it2.price }
                }

                it.selection?.let { it1 -> total += it1.price }
            }

            return total * quantity
        }

    fun stringifyOptions(): String {
        val namedOptions = ArrayList<String>()
        options.forEach {
            it.selection?.let { it1 -> namedOptions.add(it1.name) }
            it.selections?.let { it1 ->
                it1.forEach { it2 -> namedOptions.add(it2.name) }
            }
        }
        return namedOptions.joinToString()
    }

}

@Entity
data class OrderItemOption (

        @ManyToOne
        var option: ItemOption,

        @ManyToOne
        var selection: ItemOptionItem?,

        @ManyToMany(fetch = FetchType.EAGER)
        var selections: MutableList<ItemOptionItem>?

) : BaseEntity()