package com.aceinteract.blipsit.entity

import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.OneToMany

@Entity
data class Item (

        var name: String,
        var price: Double,

        @ManyToOne
        var category: ItemCategory,

        var durationInMinutes: Int,

        var description: String = "",

        @ManyToOne
        var merchant: Merchant,

        @OneToMany
        var options: MutableList<ItemOption> = ArrayList()

) : BaseEntity()

@Entity
data class ItemCategory (

        var name: String,

        @OneToMany(mappedBy = "category")
        var items: MutableList<Item> = ArrayList()

) : BaseEntity()

@Entity
data class ItemOption (

        var title: String,

        var optional: Boolean,

        @OneToMany
        var items: MutableList<ItemOptionItem> = ArrayList()

) : BaseEntity()

@Entity
data class ItemOptionItem (

        var name: String,
        var price: Double

) : BaseEntity()