package com.aceinteract.blipsit.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ManyToOne

@Entity
data class PaymentTransaction (

        val reference: String,

        @ManyToOne
        val account: Account,

        @Column(columnDefinition="TEXT", length = 2000)
        var verificationJson: String = "",

        var status: String = TRANSACTION_INITIALIZED

) : BaseEntity() {

    companion object {
        const val TRANSACTION_INITIALIZED = "TRANSACTION_INITIALIZED"
        const val TRANSACTION_VERIFIED = "TRANSACTION_VERIFIED"
    }

}