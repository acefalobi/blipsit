package com.aceinteract.blipsit.entity

import java.io.Serializable
import java.time.LocalDateTime
import java.util.*
import javax.persistence.Id
import javax.persistence.MappedSuperclass

@MappedSuperclass
abstract class BaseEntity : Serializable {

    @Id
    val id: String = UUID.randomUUID().toString()
    val dateAdded: LocalDateTime = LocalDateTime.now()

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null) return false
        return id == (other as BaseEntity).id
    }

    companion object {
        private const val serialVersionUID = 1L
    }

}