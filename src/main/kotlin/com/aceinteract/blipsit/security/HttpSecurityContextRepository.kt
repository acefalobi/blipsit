package com.aceinteract.blipsit.security

import com.aceinteract.blipsit.repository.AccountRepository
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.web.context.HttpRequestResponseHolder
import org.springframework.security.web.context.HttpSessionSecurityContextRepository



class HttpSecurityContextRepository(private val accountRepository: AccountRepository)
    : HttpSessionSecurityContextRepository() {

    override fun loadContext(requestResponseHolder: HttpRequestResponseHolder?): SecurityContext {
        val context = super.loadContext(requestResponseHolder)

        val authentication = context.authentication

        if (authentication != null) {

            val updatedAccount = accountRepository.findById((authentication.principal as AccountDetails).account.id).get()
            val accountDetails = AccountDetails(updatedAccount)
            val newAuthentication = UsernamePasswordAuthenticationToken(accountDetails, accountDetails.password, authentication.authorities)

            context.authentication = newAuthentication

        }

        return context
    }

}