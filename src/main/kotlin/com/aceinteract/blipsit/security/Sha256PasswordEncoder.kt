package com.aceinteract.blipsit.security

import com.aceinteract.blipsit.util.getHashValue
import org.springframework.security.crypto.password.PasswordEncoder

class Sha256PasswordEncoder : PasswordEncoder {

    override fun encode(password: CharSequence?): String = getHashValue("SHA-256", password as String)

    override fun matches(password1: CharSequence?, encodedPassword: String?): Boolean =
            encode(password1) == encodedPassword

}