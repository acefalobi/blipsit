package com.aceinteract.blipsit.security

import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class AuthenticationEntryPoint(loginFormUrl: String) : LoginUrlAuthenticationEntryPoint(loginFormUrl) {

    override fun commence(request: HttpServletRequest?, response: HttpServletResponse?, authException: AuthenticationException?) {
        response!!.sendRedirect("""$loginFormUrl?url_from=${request!!.requestURL}""")
    }

}