package com.aceinteract.blipsit.security

import com.aceinteract.blipsit.entity.Account
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

class AccountDetails(var account: Account) : UserDetails {

    override fun getAuthorities(): MutableCollection<out GrantedAuthority>? = null

    override fun isEnabled(): Boolean = account.isActive

    override fun getUsername(): String = account.email

    override fun isCredentialsNonExpired(): Boolean = true

    override fun getPassword(): String = account.password

    override fun isAccountNonExpired(): Boolean = account.isActive

    override fun isAccountNonLocked(): Boolean = !account.isLocked

}