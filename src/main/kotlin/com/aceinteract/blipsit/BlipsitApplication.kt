package com.aceinteract.blipsit

import com.aceinteract.blipsit.config.FileStorageProperties
import com.aceinteract.blipsit.repository.*
import com.aceinteract.blipsit.util.fillMerchantCategories
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafProperties
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.event.EventListener
import org.thymeleaf.templateresolver.FileTemplateResolver
import org.thymeleaf.templateresolver.ITemplateResolver

@SpringBootApplication
@EnableConfigurationProperties(FileStorageProperties::class)
class BlipsitApplication {

    @Autowired
    private lateinit var itemRepository: ItemRepository

    @Autowired
    private lateinit var itemCategoryRepository: ItemCategoryRepository

    @Autowired
    private lateinit var itemOptionRepository: ItemOptionRepository

    @Autowired
    private lateinit var itemOrderItemRepository: ItemOptionItemRepository

    @Autowired
    private lateinit var merchantCategoryRepository: MerchantCategoryRepository

    @Autowired
    private lateinit var merchantRepository: MerchantRepository

    @Autowired
    private lateinit var thymeleafProperties: ThymeleafProperties

    @Value("\${spring.thymeleaf.templates_root:}")
    private val templatesRoot: String? = null

    @Bean
    fun defaultTemplateResolver(): ITemplateResolver {
        val resolver = FileTemplateResolver()
        resolver.suffix = thymeleafProperties.suffix
        resolver.prefix = templatesRoot
        resolver.setTemplateMode(thymeleafProperties.mode)
        resolver.isCacheable = thymeleafProperties.isCache
        return resolver
    }

    @EventListener(ApplicationReadyEvent::class)
    fun fillDatabase() {

        fillMerchantCategories(merchantCategoryRepository)

//        fillItemCategory(itemCategoryRepository)

//        fillItems(itemRepository, itemOptionRepository, itemOrderItemRepository, merchantRepository, itemCategoryRepository)

    }

    companion object {

        const val STANDARD_DELIVERY_FEE = 200.0

        const val DELIVERY_MULTIPLIER = 1.0

        const val MAX_DELIVERY_DISTANCE = 12000

        const val GOOGLE_API_KEY = "AIzaSyC1wSkKhptdJWY7m1PZWD5LJspToZU12m8"

    }

}

fun main(args: Array<String>) {
    runApplication<BlipsitApplication>(*args)
}
