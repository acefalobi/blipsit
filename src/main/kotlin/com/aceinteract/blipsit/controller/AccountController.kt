package com.aceinteract.blipsit.controller

import com.aceinteract.blipsit.entity.*
import com.aceinteract.blipsit.exception.FileNotFoundException
import com.aceinteract.blipsit.model.AddToBasket
import com.aceinteract.blipsit.repository.*
import com.aceinteract.blipsit.security.AccountDetails
import com.aceinteract.blipsit.service.AppUserDetailsService
import com.aceinteract.blipsit.service.FileStorageService
import com.aceinteract.blipsit.util.validateAccountCreation
import com.aceinteract.blipsit.util.validateCardCreation
import com.aceinteract.blipsit.util.validateCheckout
import com.aceinteract.blipsit.util.validateCheckoutPayment
import me.iyanuadelekan.paystackjava.core.Transactions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import java.io.IOException
import javax.servlet.http.HttpServletRequest

@Controller()
@RequestMapping("/account")
class AccountController : BaseController() {

    @Autowired
    private lateinit var fileStorageService: FileStorageService

    @Autowired
    private lateinit var userDetailsService: AppUserDetailsService

    @Autowired
    private lateinit var accountRepository: AccountRepository

    @Autowired
    private lateinit var cardRepository: CardRepository

    @Autowired
    private lateinit var basketRepository: BasketRepository

    @Autowired
    private lateinit var addressRepository: AddressRepository

    @Autowired
    private lateinit var itemRepository: ItemRepository

    @Autowired
    private lateinit var itemOptionRepository: ItemOptionRepository

    @Autowired
    private lateinit var itemOptionItemRepository: ItemOptionItemRepository

    @Autowired
    private lateinit var orderRepository: OrderRepository

    @Autowired
    private lateinit var orderItemOptionRepository: OrderItemOptionRepository

    @Autowired
    private lateinit var paymentTransactionRepository: PaymentTransactionRepository

    @GetMapping("/login")
    fun getLoginView(): String = "account/login"

    @GetMapping("/create")
    fun createAccount(
            model: Model,
            request: HttpServletRequest,
            @RequestParam(required = false, name = "url_from") urlFrom: String?
    ): String {
        return if (urlFrom != null) "account/create"
        else {
            if (request.getHeader("referer") == null) "redirect:/account/create?url_from="
            else "redirect:/account/create?url_from=${request.getHeader("referer")}"
        }
    }

    @PostMapping("/create")
    fun createAccount(model: Model, @RequestParam urlFrom: String, @RequestParam email: String, @RequestParam password: String,
                      @RequestParam fullName: String): String {
        val errorMessage = validateAccountCreation(accountRepository, email, fullName, password)
        return if (errorMessage.isNotEmpty()) {
            model.addAttribute("errorMessage", errorMessage)
            "account/create"
        } else{
            val account = Account(email, password, fullName)
            accountRepository.save(account)

            val accountDetails = userDetailsService.loadUserByUsername(account.email)
            SecurityContextHolder.getContext().authentication = UsernamePasswordAuthenticationToken(
                    accountDetails.username, accountDetails.password, accountDetails.authorities
            )

            if (urlFrom.isEmpty()) "redirect:/"
            else "redirect:$urlFrom"
        }
    }

    @GetMapping("/avatar/{accountId}")
    fun getAvatar(@PathVariable accountId: String, request: HttpServletRequest): ResponseEntity<Resource> {

        val resource: Resource = try {
            fileStorageService.loadFileAsResource("avatar/$accountId.jpg")
        } catch (exception: FileNotFoundException) {
            fileStorageService.loadFileAsResource("avatar/default.png")
        }

        var contentType = "image/jpeg"
        try {
            contentType = request.servletContext.getMimeType(resource.file.absolutePath)
        } catch (exception: IOException) {
            logger.info("Could not determine file type. Using default file type")
        }

        return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\" ${resource.filename} \"")
                .body(resource)
    }

    @PostMapping("/card/add")
    fun addCard(request: HttpServletRequest, authentication: Authentication?, model: Model, @RequestParam cardName: String,
                @RequestParam cardNumber: String, @RequestParam expiryDate: String, @RequestParam cvv: String): String {

        val account = (authentication!!.principal as AccountDetails).account

        val errorMessage = validateCardCreation(cardName, cardNumber, expiryDate)

        if (errorMessage.isNotEmpty()) {

            return if (request.getHeader("referer") == null) "redirect:/"
            else "redirect:${request.getHeader("referer")}?error"

        } else {

            val card = Card(account, cardName, cardNumber.replace(" ", ""), cvv,
                    expiryDate.substring(0, 2), expiryDate.substring(3))

            cardRepository.save(card)

            return if (request.getHeader("referer") == null) "redirect:/"
            else "redirect:${request.getHeader("referer")}"

        }

    }

    @PostMapping("/basket/add")
    fun addToBasket(authentication: Authentication?, @RequestBody addToBasket: AddToBasket): ResponseEntity<String> {

        val basket = accountRepository.
                findById((authentication!!.principal as AccountDetails).account.id).get().basket

        val item = itemRepository.findById(addToBasket.itemId).get()
        val itemOptions = ArrayList<OrderItemOption>()

        addToBasket.options.forEach { it ->
            val itemOption = itemOptionRepository.findById(it.optionId).get()
            if (it.optional) {
                val selections = ArrayList<ItemOptionItem>()
                it.optionItemIds!!.forEach {
                    selections.add(itemOptionItemRepository.findById(it).get())
                }
                itemOptions.add(orderItemOptionRepository.save(OrderItemOption(itemOption, null, selections)))
            } else {
                val selection = itemOptionItemRepository.findById(it.optionItemId!!).get()
                itemOptions.add(orderItemOptionRepository.save(OrderItemOption(itemOption, selection, null)))
            }
        }

        val orderItem = OrderItem(item, itemOptions, addToBasket.quantity, basket = basket)
        orderItemRepository.save(orderItem)

        return ResponseEntity.ok().body("OK")

    }

    @GetMapping("/basket/remove/{orderItemId}")
    fun removeFromBaset(request: HttpServletRequest, authentication: Authentication?, @PathVariable orderItemId: String): String {

        val orderItem = orderItemRepository.findById(orderItemId).get()

        if ((authentication!!.principal as AccountDetails).account.id.equals(orderItem.basket!!.consumer.id))
                orderItemRepository.deleteById(orderItemId)

        return if (request.getHeader("referer") == null) "redirect:/"
        else "redirect:${request.getHeader("referer")}"

    }

    @GetMapping("/basket/checkout")
    fun checkout(authentication: Authentication?): String {
        return if ((authentication!!.principal as AccountDetails).account.basket.items.isEmpty()) "redirect:/"
        else "account/basket/checkout"

    }

    @PostMapping("/basket/checkout")
    fun checkout(authentication: Authentication?, model: Model, @RequestParam addressId: String): String {

        val account = (authentication!!.principal as AccountDetails).account

        val address = addressRepository.findById(addressId).get()

        val errorMessage = validateCheckout(account.basket, address)

        if (errorMessage.isNotEmpty()) {
            model.addAttribute("errorMessage", errorMessage)
        } else {
            val order = ConsumerOrder(account, ConsumerOrder.PAYMENT_CARD, address)
            orderRepository.save(order)

            account.basket.items.forEach {
                it.basket = null
                it.order = order
                orderItemRepository.save(it)
            }

            return "redirect:/order/find-courier"
        }

        return "account/basket/checkout"

    }

    @GetMapping("/basket/checkout/verify-payment/")
    fun checkoutVerifyPayment(authentication: Authentication?, @RequestParam reference: String): String {

        val account = (authentication!!.principal as AccountDetails).account
        val paymentTransaction = paymentTransactionRepository.findByReference(reference)!!

        return if (validateCheckoutPayment(orderItemRepository, paymentTransaction, account, reference).isNotEmpty())
            "redirect:/account/basket/checkout?error"
        else {

            val transactions = Transactions()
            val transaction = transactions.verifyTransaction(reference)

            val transactionData = transaction.getJSONObject("data")

            val customFields = transactionData.getJSONObject("metadata")
                    .getJSONArray("custom_fields").getJSONObject(0)

            val address = addressRepository.findById(customFields.getString("addressId")).get()

            paymentTransaction.verificationJson = transaction.toString()
            paymentTransaction.status = PaymentTransaction.TRANSACTION_VERIFIED
            paymentTransactionRepository.save(paymentTransaction)

            return if (validateCheckout(account.basket, address).isNotEmpty()) {
                "redirect:/account/basket/checkout?error"
            } else {
                val order = ConsumerOrder(account, ConsumerOrder.PAYMENT_CARD, address)
                orderRepository.save(order)

                account.basket.items.forEach {
                    it.basket = null
                    it.order = order
                    orderItemRepository.save(it)
                }

                "redirect:/order/find-courier"
            }
        }

    }

    @PostMapping("/address/add")
    fun addAddress(request: HttpServletRequest, authentication: Authentication?, @RequestParam address: String,
                   @RequestParam phoneNumber: String, @RequestParam(defaultValue = "") extraInfo: String,
                   @RequestParam latitude: Double, @RequestParam longitude: Double): String {
        val account = (authentication!!.principal as AccountDetails).account

        addressRepository.save(Address(account, latitude, longitude, address, extraInfo, phoneNumber))

        return if (request.getHeader("referer") == null) "redirect:/"
        else "redirect:${request.getHeader("referer")}"
    }

}