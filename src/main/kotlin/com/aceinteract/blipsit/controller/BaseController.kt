package com.aceinteract.blipsit.controller

import com.aceinteract.blipsit.BlipsitApplication
import com.aceinteract.blipsit.repository.OrderItemRepository
import com.aceinteract.blipsit.security.AccountDetails
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.Authentication
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.ModelAttribute

open class BaseController {

    @Autowired
    lateinit var orderItemRepository: OrderItemRepository

    @ModelAttribute
    fun addAttributes(model: Model, authentication: Authentication?) {
        model.addAttribute("DELIVERY_MULTIPLIER", BlipsitApplication.DELIVERY_MULTIPLIER)
        model.addAttribute("DELIVERY_FEE", BlipsitApplication.STANDARD_DELIVERY_FEE * BlipsitApplication.DELIVERY_MULTIPLIER)
        if (authentication != null) {
            val account = (authentication.principal as AccountDetails).account
            val basketItems = orderItemRepository.findAllByBasket(account.basket)

            val basketSubTotal = account.basket.getSubTotal(orderItemRepository)

            model.addAttribute("basketItems", basketItems)
            model.addAttribute("basketSubTotal", basketSubTotal)
        }
    }

    companion object {
        val logger = LoggerFactory.getLogger(this::class.java)!!
    }
}