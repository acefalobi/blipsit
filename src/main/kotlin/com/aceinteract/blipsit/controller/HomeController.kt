package com.aceinteract.blipsit.controller

import org.springframework.security.core.Authentication
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import javax.servlet.http.HttpServletRequest


@Controller
class HomeController : BaseController() {

    @GetMapping("/")
    fun index(model: Model): String {
        return "home/landing"
    }

    @GetMapping("/find")
    fun find(request: HttpServletRequest, authentication: Authentication?, model: Model): String {
        return if (request.cookies.find { it.name == "longitude" } != null &&
                request.cookies.find { it.name == "latitude" } != null) {
            "home/find"
        } else "redirect:/"
    }

}