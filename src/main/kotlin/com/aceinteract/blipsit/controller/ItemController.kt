package com.aceinteract.blipsit.controller

import com.aceinteract.blipsit.exception.FileNotFoundException
import com.aceinteract.blipsit.service.FileStorageService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import java.io.IOException
import javax.servlet.http.HttpServletRequest

@Controller
@RequestMapping("/item")
class ItemController : BaseController() {

    @Autowired
    private lateinit var fileStorageService: FileStorageService


    @GetMapping("/image/{itemId}")
    fun getImage(@PathVariable itemId: String, request: HttpServletRequest): ResponseEntity<Resource> {

        val resource: Resource = try {
            fileStorageService.loadFileAsResource("item-image/$itemId.jpg")
        } catch (exception: FileNotFoundException) {
            fileStorageService.loadFileAsResource("item-image/default.png")
        }

        var contentType = "image/jpeg"
        try {
            contentType = request.servletContext.getMimeType(resource.file.absolutePath)
        } catch (exception: IOException) {
            logger.info("Could not determine file type. Using default file type")
        }

        return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\" ${resource.filename} \"")
                .body(resource)

    }

}