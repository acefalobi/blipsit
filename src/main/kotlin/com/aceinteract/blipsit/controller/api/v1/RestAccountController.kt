package com.aceinteract.blipsit.controller.api.v1

import com.aceinteract.blipsit.BlipsitApplication
import com.aceinteract.blipsit.entity.PaymentTransaction
import com.aceinteract.blipsit.model.AddressDeliveryResponse
import com.aceinteract.blipsit.model.InitializeTransactionResponse
import com.aceinteract.blipsit.repository.AddressRepository
import com.aceinteract.blipsit.repository.PaymentTransactionRepository
import com.aceinteract.blipsit.security.AccountDetails
import com.aceinteract.blipsit.util.getDistanceInMetres
import com.aceinteract.blipsit.exception.NotAuthorizedException
import com.google.maps.GeoApiContext
import com.google.maps.model.LatLng
import me.iyanuadelekan.paystackjava.core.ApiQuery
import me.iyanuadelekan.paystackjava.core.Transactions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.rest.webmvc.ResourceNotFoundException
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("${BaseApiV1Controller.baseUrl}/account")
class RestAccountController : BaseApiV1Controller() {

    @Autowired
    private lateinit var addressRepository: AddressRepository

    @Autowired
    private lateinit var paymentTransactionRepository: PaymentTransactionRepository


    @GetMapping("/address/delivery/{addressId}")
    fun getAddressDelivery(authentication: Authentication?, @PathVariable addressId: String): AddressDeliveryResponse {

        val address = addressRepository.findById(addressId)
        if (!address.isPresent) throw ResourceNotFoundException()

        val account = (authentication!!.principal as AccountDetails).account

        if (account.id != address.get().account.id) throw NotAuthorizedException()

        val geoApiContext = GeoApiContext.Builder().apiKey(BlipsitApplication.GOOGLE_API_KEY).build()

        val origin = LatLng(account.basket.merchant!!.latitude, account.basket.merchant!!.longitude)
        val destination = LatLng(address.get().latitude, address.get().longitude)

        val distanceInMetres = getDistanceInMetres(geoApiContext, origin, destination)

        return AddressDeliveryResponse(address.get(), BlipsitApplication.DELIVERY_MULTIPLIER,
                BlipsitApplication.STANDARD_DELIVERY_FEE, account.basket.getTotal(orderItemRepository),
                distanceInMetres < BlipsitApplication.MAX_DELIVERY_DISTANCE)
    }

    @GetMapping("/basket/checkout/initialize-payment")
    fun initializeCheckoutPayment(authentication: Authentication?, @RequestParam addressId: String): InitializeTransactionResponse {
        val account = (authentication!!.principal as AccountDetails).account

        val amount = account.basket.getTotal(orderItemRepository) * 100

        val paymentTransaction = PaymentTransaction(System.currentTimeMillis().toString(), account)
        paymentTransactionRepository.save(paymentTransaction)

        val apiQuery = ApiQuery()
        apiQuery.putParams("reference", paymentTransaction.reference)
        apiQuery.putParams("amount", amount.toString())
        apiQuery.putParams("email", account.email)
        apiQuery.putParams("callback_url", "http://localhost:8080/account/basket/checkout/verify-payment/")
        apiQuery.putParams("metadata",
                "{\"custom_fields\": [{\"addressId\": \"$addressId\"}]}")

        val transactions = Transactions()
        val transaction = transactions.initializeTransaction(apiQuery)

        if (!transaction.getBoolean("status")) throw Exception("Unable to initiate transaction")

        val transactionData = transaction.getJSONObject("data")

        return InitializeTransactionResponse(transactionData.getString("authorization_url"),
                transactionData.getString("access_code"), transactionData.getString("reference"))
    }

}