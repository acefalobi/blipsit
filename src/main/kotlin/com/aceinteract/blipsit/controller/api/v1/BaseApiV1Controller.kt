package com.aceinteract.blipsit.controller.api.v1

import com.aceinteract.blipsit.controller.BaseController

open class BaseApiV1Controller : BaseController() {

    companion object {

        const val baseUrl = "/api/v1"

    }

}