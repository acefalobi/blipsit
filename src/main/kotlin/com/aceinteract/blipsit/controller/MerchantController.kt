package com.aceinteract.blipsit.controller

import com.aceinteract.blipsit.repository.MerchantRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/merchant")
class MerchantController : BaseController() {

    @Autowired
    private lateinit var merchantRepository: MerchantRepository

    @GetMapping("/{merchantSlug}")
    fun getMerchant(model: Model, @PathVariable merchantSlug: String): String {

        val merchant = merchantRepository.findBySlug(merchantSlug)

        model.addAttribute(merchant!!)

        return "merchant/detail"

    }

}