package com.aceinteract.blipsit.config

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "file")
class FileStorageProperties {

    var uploadDir: String = ""

}