package com.aceinteract.blipsit.config

import com.aceinteract.blipsit.repository.AccountRepository
import com.aceinteract.blipsit.security.AuthenticationEntryPoint
import com.aceinteract.blipsit.security.HttpSecurityContextRepository
import com.aceinteract.blipsit.security.Sha256PasswordEncoder
import com.aceinteract.blipsit.service.AppUserDetailsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint


@Configuration
@EnableWebSecurity
class SecurityConfig : WebSecurityConfigurerAdapter() {

    @Autowired
    private lateinit var userDetailsService: AppUserDetailsService

    @Autowired
    private lateinit var accountRepository: AccountRepository

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {

        http.authorizeRequests()
                .antMatchers(
                        "/static/**",
                        "/",
                        "/account/create",
                        "/account/login",
                        "/account/avatar/**",
                        "/item/image/**"
                ).permitAll().anyRequest().authenticated()
                .and()
                .formLogin().loginPage("/account/login").defaultSuccessUrl("/").permitAll()
                .and()
                .logout().permitAll()
                .logoutUrl("/account/logout")
                .and()
                .exceptionHandling().authenticationEntryPoint(authenticationEntryPoint())
                .and()
                .csrf().disable()
                .securityContext().securityContextRepository(HttpSecurityContextRepository(accountRepository))
    }

    @Throws(Exception::class)
    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.authenticationProvider(authenticationProvider())
    }

    @Bean
    fun authenticationProvider(): DaoAuthenticationProvider {
        val authProvider = DaoAuthenticationProvider()
        authProvider.setUserDetailsService(userDetailsService)
        authProvider.setPasswordEncoder(encoder())
        return authProvider
    }

    @Bean
    fun encoder(): PasswordEncoder {
        return Sha256PasswordEncoder()
    }

    fun authenticationEntryPoint() : LoginUrlAuthenticationEntryPoint {
        return AuthenticationEntryPoint("/account/login")
    }

}