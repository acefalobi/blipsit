package com.aceinteract.blipsit.service

import com.aceinteract.blipsit.repository.AccountRepository
import com.aceinteract.blipsit.security.AccountDetails
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class AppUserDetailsService : UserDetailsService {

    @Autowired
    private lateinit var accountRepository: AccountRepository

    override fun loadUserByUsername(email: String): UserDetails {
        val account = accountRepository.findByEmail(email) ?: throw UsernameNotFoundException(email)

        return AccountDetails(account)
    }

}