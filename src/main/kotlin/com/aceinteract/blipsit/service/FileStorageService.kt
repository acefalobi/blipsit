package com.aceinteract.blipsit.service

import com.aceinteract.blipsit.config.FileStorageProperties
import com.aceinteract.blipsit.exception.FileNotFoundException
import com.aceinteract.blipsit.exception.FileStorageException
import org.springframework.core.io.Resource
import org.springframework.core.io.UrlResource
import org.springframework.stereotype.Service
import org.springframework.util.StringUtils
import org.springframework.web.multipart.MultipartFile
import java.io.IOException
import java.net.MalformedURLException
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardCopyOption

@Service
class FileStorageService(fileStorageProperties: FileStorageProperties) {

    private val fileStorageLocation = Paths.get(fileStorageProperties.uploadDir).toAbsolutePath().normalize()

    init {
        try {
            Files.createDirectories(this.fileStorageLocation)
            Files.createDirectories(Paths.get(this.fileStorageLocation.toString(), "avatar"))
            Files.createDirectories(Paths.get(this.fileStorageLocation.toString(), "item-image"))
        } catch (exception: Exception) {
            throw FileStorageException("Could not create the directory where the uploaded files will be stored.", exception)
        }
    }

    fun storeFile(file: MultipartFile, targetFolder: String): String {
        val fileName = StringUtils.cleanPath("$targetFolder/${file.originalFilename!!}")

        try {
            if (fileName.contains("..")) throw FileStorageException("Sorry! Filename contains invalid path sequence $fileName")

            val targetLocation = this.fileStorageLocation.resolve(fileName)
            Files.copy(file.inputStream, targetLocation, StandardCopyOption.REPLACE_EXISTING)

            return fileName
        } catch (exception: IOException) {
            throw FileStorageException("Could not store file $fileName. Please try again!", exception)
        }
    }

    fun loadFileAsResource(fileName: String): Resource {
        try {
            val filePath = this.fileStorageLocation.resolve(fileName).normalize()
            val resource = UrlResource(filePath.toUri())

            if (resource.exists()) return resource
            else throw FileNotFoundException("File not found $fileName")
        } catch (exception: MalformedURLException) {
            throw FileNotFoundException("File not found $fileName")
        }
    }

}