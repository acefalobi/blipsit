package com.aceinteract.blipsit.util

import com.aceinteract.blipsit.entity.*
import com.aceinteract.blipsit.repository.*


fun fillItemCategory(itemCategoryRepository: ItemCategoryRepository) {

    itemCategoryRepository.save(ItemCategory("Tacos"))

}

fun fillItems(itemRepository: ItemRepository, itemOptionRepository: ItemOptionRepository,
              itemOptionItemRepository: ItemOptionItemRepository, merchantRepository: MerchantRepository,
              itemCategoryRepository: ItemCategoryRepository) {

    val merchant = merchantRepository.findBySlug("the-place")!!

    val category = itemCategoryRepository.save(ItemCategory("Tacos"))
    merchant.itemCategories.add(category)

    merchantRepository.save(merchant)

    val item = Item("Regular Taco", 800.0, category, 20,
            "That's a spicy meatball!", merchant)
    itemRepository.save(item)

    val meatOption = ItemOption("Meat", false)
    itemOptionRepository.save(meatOption)
    val meatOptionItems = ArrayList<ItemOptionItem>()
    meatOptionItems.add(ItemOptionItem("Birris (Shredded Beef)", 0.0))
    meatOptionItems.add(ItemOptionItem("Lengua (Tongue)", 250.0))
    meatOptionItems.add(ItemOptionItem("Cabeza (Cheek)", 0.0))
    meatOptionItems.add(ItemOptionItem("Al Pastor (BBQ Pork)", 0.0))
    meatOptionItems.add(ItemOptionItem("Asada (Steak)", 0.0))
    meatOptionItems.add(ItemOptionItem("Pollo (Chicken)", 0.0))
    meatOptionItems.add(ItemOptionItem("No Meat", 0.0))
    itemOptionItemRepository.saveAll(meatOptionItems)
    meatOption.items = meatOptionItems
    itemOptionRepository.save(meatOption)

    val toppingsOption = ItemOption("Toppings", true)
    itemOptionRepository.save(toppingsOption)
    val toppingsOptionItems = ArrayList<ItemOptionItem>()
    toppingsOptionItems.add(ItemOptionItem("Salsa Roja (Red Spicy)", 0.0))
    toppingsOptionItems.add(ItemOptionItem("Salsa Verde (Green Mild)", 0.0))
    toppingsOptionItems.add(ItemOptionItem("No Toppings", 0.0))
    toppingsOptionItems.add(ItemOptionItem("Todo (Everything)", 0.0))
    toppingsOptionItems.add(ItemOptionItem("Onion", 0.0))
    toppingsOptionItems.add(ItemOptionItem("Cilantro", 0.0))
    itemOptionItemRepository.saveAll(toppingsOptionItems)
    toppingsOption.items = toppingsOptionItems
    itemOptionRepository.save(toppingsOption)

    val sizeOption = ItemOption("Size", false)
    itemOptionRepository.save(sizeOption)
    val sizeOptionItems = ArrayList<ItemOptionItem>()
    sizeOptionItems.add(ItemOptionItem("Small", 0.0))
    sizeOptionItems.add(ItemOptionItem("Medium", 150.0))
    sizeOptionItems.add(ItemOptionItem("Large", 200.0))
    sizeOptionItems.add(ItemOptionItem("Large", 250.0))
    itemOptionItemRepository.saveAll(sizeOptionItems)
    sizeOption.items = sizeOptionItems
    itemOptionRepository.save(sizeOption)

    item.options.add(meatOption)
    item.options.add(toppingsOption)
    item.options.add(sizeOption)
    itemRepository.save(item)

}

fun fillMerchantCategories(merchantCategoryRepository: MerchantCategoryRepository) {

    val categories = ArrayList<MerchantCategory>()

    if (!merchantCategoryRepository.existsBySlug("african"))
        categories.add(MerchantCategory("African", "african", ""))

    if (!merchantCategoryRepository.existsBySlug("coffee"))
        categories.add(MerchantCategory("Coffee", "coffee", ""))

    if (!merchantCategoryRepository.existsBySlug("western"))
        categories.add(MerchantCategory("Western", "western", ""))

    if (!merchantCategoryRepository.existsBySlug("pizza"))
        categories.add(MerchantCategory("Pizza", "pizza", ""))

    if (!merchantCategoryRepository.existsBySlug("fast-food"))
        categories.add(MerchantCategory("Fast Food", "fast-food", ""))

    if (!merchantCategoryRepository.existsBySlug("chinese"))
        categories.add(MerchantCategory("Chinese", "chinese", ""))

    if (!merchantCategoryRepository.existsBySlug("bakery"))
        categories.add(MerchantCategory("Bakery", "bakery", ""))

    if (!merchantCategoryRepository.existsBySlug("grocery"))
        categories.add(MerchantCategory("Grocery", "grocery", ""))

    if (!merchantCategoryRepository.existsBySlug("exotic"))
        categories.add(MerchantCategory("Exotic", "exotic", ""))

    if (!merchantCategoryRepository.existsBySlug("salad"))
        categories.add(MerchantCategory("Salad", "salad", ""))

    merchantCategoryRepository.saveAll(categories)

}