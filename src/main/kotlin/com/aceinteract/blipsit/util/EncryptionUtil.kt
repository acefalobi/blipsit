package com.aceinteract.blipsit.util

import java.security.MessageDigest

fun getHashValue(algorithm: String, message: String): String {
    val messageBytes = message.toByteArray()
    val messageDigest = MessageDigest.getInstance(algorithm)
    val digest = messageDigest.digest(messageBytes)
    return digest.fold("") { str, it -> str + "%02x".format(it) }
}