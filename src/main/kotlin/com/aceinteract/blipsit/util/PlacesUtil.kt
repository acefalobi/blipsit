package com.aceinteract.blipsit.util

import com.google.maps.DistanceMatrixApiRequest
import com.google.maps.GeoApiContext
import com.google.maps.PendingResult
import com.google.maps.model.DistanceMatrix
import com.google.maps.model.LatLng
import com.google.maps.model.TravelMode
import com.google.maps.model.Unit

fun getDistanceInMetres(geoApiContext: GeoApiContext, origin: LatLng, destination: LatLng): Long {

    var hasFinishedRequest = false

    var distanceMatrix: DistanceMatrix? = null

    DistanceMatrixApiRequest(geoApiContext)
            .origins(origin).destinations(destination).mode(TravelMode.DRIVING).units(Unit.METRIC)
            .setCallback(object : PendingResult.Callback<DistanceMatrix> {
                override fun onFailure(error: Throwable?) {
                    throw Exception(error)
                }

                override fun onResult(p0: DistanceMatrix?) {
                    hasFinishedRequest = true
                    distanceMatrix = p0
                }

            })

    while (!hasFinishedRequest) {
        Thread.sleep(50)
    }

    var distanceInMetres: Long = 0

    if (distanceMatrix != null ) {
        val distance = distanceMatrix!!.rows[0].elements[0].distance
        distanceInMetres = distance?.inMeters ?: coordinateDistance(origin.lat, destination.lat, origin.lng, destination.lng).toLong()
    }

    return distanceInMetres

}