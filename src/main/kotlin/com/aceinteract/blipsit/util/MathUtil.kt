package com.aceinteract.blipsit.util

import com.aceinteract.blipsit.util.MathUtil.Companion.R

fun coordinateDistance(lat1: Double, lat2: Double, lon1: Double, lon2: Double): Double {

    val latDistance = Math.toRadians(lat2 - lat1)
    val lonDistance = Math.toRadians(lon2 - lon1)
    val a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + (Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
            * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2))
    val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))

    var distance = R.toDouble() * c * 1000.0

    distance = Math.pow(distance, 2.0)

    return Math.sqrt(distance)
}

class MathUtil {

    companion object {
        const val R = 6371
    }

}