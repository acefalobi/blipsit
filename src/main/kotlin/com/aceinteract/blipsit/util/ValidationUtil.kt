package com.aceinteract.blipsit.util

import com.aceinteract.blipsit.BlipsitApplication
import com.aceinteract.blipsit.entity.Account
import com.aceinteract.blipsit.entity.Address
import com.aceinteract.blipsit.entity.Basket
import com.aceinteract.blipsit.entity.PaymentTransaction
import com.aceinteract.blipsit.repository.AccountRepository
import com.aceinteract.blipsit.repository.OrderItemRepository
import com.google.maps.GeoApiContext
import com.google.maps.model.LatLng
import me.iyanuadelekan.paystackjava.core.Transactions

fun validateAccountCreation(accountRepository: AccountRepository,
                            email: String, fullName: String, password: String): String {
    if (email.trim().isEmpty() || fullName.trim().isEmpty() || password.trim().isEmpty())
        return "Please enter all required fields"
    if (password.length < 6) return "Password must have at least 6 characters"

    if (accountRepository.findByEmail(email) != null) return "Sorry, $email is already in use"

    return ""
}

fun validateCardCreation(cardName: String, cardNumber: String, expiryDate: String): String {

    if (expiryDate.trim().isEmpty() || cardName.trim().isEmpty() || cardNumber.trim().isEmpty())
        return "Please enter all required fields"
    if (expiryDate.length > 5 || expiryDate[2].toString() != "/" || expiryDate.substring(0, 2).toInt() > 12
            || expiryDate.substring(3).toInt() < 19)
        return "Please use the proper date format"
    if (!cardNumber.matches(Regex("[0-9]+")))
        return "Please enter a valid card number"

    return ""

}

fun validateCheckout(basket: Basket, address: Address): String {

    if (address.account.id != basket.consumer.id) return "Unauthorized card or address selected"

    val geoApiContext = GeoApiContext.Builder().apiKey(BlipsitApplication.GOOGLE_API_KEY).build()

    if (basket.items.isEmpty()) return "Cannot checkout empty basket"

    val origin = LatLng(basket.merchant!!.latitude, basket.merchant!!.longitude)
    val destination = LatLng(address.latitude, address.longitude)

    if (getDistanceInMetres(geoApiContext, origin, destination) > BlipsitApplication.MAX_DELIVERY_DISTANCE)
        return "Delivery address is too far from restaurant/store"

//    if (LocalTime.now().isAfter(basket.merchant!!.closingTime) || LocalTime.now().isBefore(basket.merchant!!.openingTime))
//        return "Sorry, this merchant is already closed"

    return ""
}

fun validateCheckoutPayment(orderItemRepository: OrderItemRepository, paymentTransaction: PaymentTransaction,
                            account: Account, reference: String): String {

    if (account.id != paymentTransaction.account.id) return "Unauthorized transaction"

    if (paymentTransaction.status == PaymentTransaction.TRANSACTION_VERIFIED)
        return "Transaction is already verified"

    val transactions = Transactions()
    val transaction = transactions.verifyTransaction(reference)

    if (!transaction.getBoolean("status")) return "Unable to complete verification"

    val transactionData = transaction.getJSONObject("data")

    if (transactionData.getString("status") != "success") return "Transaction failed"

    if (transactionData.getLong("amount") != account.basket.getTotal(orderItemRepository).toLong() * 100)
        return "Unable to complete verification"

    return ""
}