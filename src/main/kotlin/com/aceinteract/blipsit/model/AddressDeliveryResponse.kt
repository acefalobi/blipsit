package com.aceinteract.blipsit.model

import com.aceinteract.blipsit.entity.Address
import com.fasterxml.jackson.annotation.JsonIgnoreProperties

data class AddressDeliveryResponse(

        @JsonIgnoreProperties("account")
        val address: Address,
        val multiplier: Double,
        val deliveryFee: Double,
        val total: Double,
        val isValid: Boolean

)