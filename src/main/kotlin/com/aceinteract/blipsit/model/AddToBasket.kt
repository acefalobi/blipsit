package com.aceinteract.blipsit.model


data class AddToBasket (

        var itemId: String,
        var quantity: Int,
        var options: MutableList<AddItemOption>

)