package com.aceinteract.blipsit.model

import java.util.*

data class AddItemOption (

        var optionId: String,
        var optional: Boolean,
        var optionItemId: String?,
        var optionItemIds: Array<String>?

) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AddItemOption

        if (optionId != other.optionId) return false
        if (optional != other.optional) return false
        if (optionItemId != other.optionItemId) return false
        if (!Arrays.equals(optionItemIds, other.optionItemIds)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = optionId.hashCode()
        result = 31 * result + optional.hashCode()
        result = 31 * result + (optionItemId?.hashCode() ?: 0)
        result = 31 * result + (optionItemIds?.let { Arrays.hashCode(it) } ?: 0)
        return result
    }
}