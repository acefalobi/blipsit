package com.aceinteract.blipsit.model

data class InitializeTransactionResponse (val authorizationUrl: String, val accessCode: String, val reference: String)