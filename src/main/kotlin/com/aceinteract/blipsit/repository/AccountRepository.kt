package com.aceinteract.blipsit.repository

import com.aceinteract.blipsit.entity.Account
import com.aceinteract.blipsit.entity.Address
import com.aceinteract.blipsit.entity.Basket
import com.aceinteract.blipsit.entity.Card
import org.springframework.stereotype.Repository

@Repository
interface AccountRepository : BaseRepository<Account> {

    fun findByEmail(email: String): Account?

}

@Repository
interface BasketRepository : BaseRepository<Basket>

@Repository
interface AddressRepository : BaseRepository<Address>

@Repository
interface CardRepository : BaseRepository<Card>