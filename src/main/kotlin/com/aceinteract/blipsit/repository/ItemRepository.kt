package com.aceinteract.blipsit.repository

import com.aceinteract.blipsit.entity.Item
import com.aceinteract.blipsit.entity.ItemCategory
import com.aceinteract.blipsit.entity.ItemOption
import com.aceinteract.blipsit.entity.ItemOptionItem
import org.springframework.stereotype.Repository

@Repository
interface ItemRepository : BaseRepository<Item>

@Repository
interface ItemCategoryRepository : BaseRepository<ItemCategory>

@Repository
interface ItemOptionRepository : BaseRepository<ItemOption>

@Repository
interface ItemOptionItemRepository : BaseRepository<ItemOptionItem>