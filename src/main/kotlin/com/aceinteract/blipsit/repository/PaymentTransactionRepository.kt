package com.aceinteract.blipsit.repository

import com.aceinteract.blipsit.entity.PaymentTransaction
import org.springframework.stereotype.Repository

@Repository
interface PaymentTransactionRepository : BaseRepository<PaymentTransaction> {

    fun findByReference(reference: String): PaymentTransaction?

}