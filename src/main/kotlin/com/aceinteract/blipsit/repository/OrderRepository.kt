package com.aceinteract.blipsit.repository

import com.aceinteract.blipsit.entity.Basket
import com.aceinteract.blipsit.entity.ConsumerOrder
import com.aceinteract.blipsit.entity.OrderItem
import com.aceinteract.blipsit.entity.OrderItemOption
import org.springframework.stereotype.Repository

@Repository
interface OrderRepository : BaseRepository<ConsumerOrder>

@Repository
interface OrderItemRepository : BaseRepository<OrderItem> {

    fun findAllByBasket(basket: Basket): MutableList<OrderItem>

}

@Repository
interface OrderItemOptionRepository : BaseRepository<OrderItemOption>