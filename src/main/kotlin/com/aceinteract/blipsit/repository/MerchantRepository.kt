package com.aceinteract.blipsit.repository

import com.aceinteract.blipsit.entity.Merchant
import com.aceinteract.blipsit.entity.MerchantCategory
import org.springframework.stereotype.Repository

@Repository
interface MerchantRepository : BaseRepository<Merchant> {

    fun findBySlug(slug: String): Merchant?

}

@Repository
interface MerchantCategoryRepository : BaseRepository<MerchantCategory> {

    fun findBySlug(slug: String): MerchantCategory?
    fun existsBySlug(slug: String): Boolean

}