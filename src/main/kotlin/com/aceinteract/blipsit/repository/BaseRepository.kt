package com.aceinteract.blipsit.repository

import com.aceinteract.blipsit.entity.BaseEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.NoRepositoryBean

@NoRepositoryBean
interface BaseRepository<T: BaseEntity> : JpaRepository<T, String>